;; Bootstrap straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el"
                         user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         (concat "https://raw.githubusercontent.com/raxod502/straight.el/"
                 "develop/install.el")
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Install use-package
(straight-use-package 'use-package)

;; Install delight, and ensure it is loaded during compile so that use-package
;; can make use of it.
(straight-use-package 'delight)
;; Hack for org-mode
(load (expand-file-name "org-mode-hack.el" user-emacs-directory))

;; Use use-package to load the rest of the packages, with straight.el as the
;; package manager.
(use-package ace-window
  :straight t
  ;; Demand because emisc-post expects it.
  :demand)
(use-package ample-regexps
  :straight t)
(use-package autorevert
  :delight)
(use-package avy
  :straight t
  ;; Demand because emisc expects it.
  :demand)
(use-package circe
  :straight t)
(use-package color-theme
  :straight t)
(use-package command-log-mode
  :straight t
  :demand
  :config
  (command-log-mode))
(use-package company
  :straight t
  :delight)
(use-package company-go
  :straight t
  :after (company go-mode))
(use-package company-jedi
  ;; Defer, letting elpy worry about when to load Jedi.
  :defer
  :straight t)
(use-package company-shell
  :straight t)
(use-package company-lsp
  :straight t
  :after (company lsp-mode)
  :commands company-lsp)
(use-package counsel
  :straight t
  ;; Demand here so that, e.g. M-y uses counsel-yank-pop right off the bat.
  :demand
  :delight
  ;; :init
  ;; (defun counsel-rg-here ()
  ;;   (interactive)
  ;;   (counsel-rg nil default-directory nil nil))
  :bind (("s-4" .  counsel-git)
         ("s-5" .  counsel-git-grep)
         ("s-6" .  counsel-rg)     ; install ripgrep or use counsel-ag
         ("s-7" .  emisc-counsel-locate-here)
         ("s-&" .  counsel-locate)
         ("s-8" .  counsel-linux-app)
         ("s-f" .  counsel-find-file)
         ("s-i" .  emisc-counsel-bookmark)
         ("s-x" .  counsel-M-x)
         ("M-s-u" . counsel-unicode-char)
         ("C-h b" . nil)
         ("M-s-b" . counsel-descbinds)
         ("C-h f" . nil)
         ("M-s-f" . counsel-describe-function)
         ("C-h v" . nil)
         ("M-s-v" . counsel-describe-variable)))
(use-package cus-edit
  :bind (("C-s-g" . customize-option)
         ("C-s-S-g" . customize-group)
         :map custom-field-keymap
         ("s-s" . Custom-save)
         :map custom-mode-map
         ("s-s" . Custom-save)))
(use-package cython-mode
  :defer
  :straight t)
(use-package diary-lib
  :straight t)
(use-package dired
  :bind (:map dired-mode-map
         ;; ("RET" . (lambda ()
         ;;            (interactive)
         ;;            (if (dired-utils-is-dir-p)
         ;;                (when (y-or-n-p "really use enter? ")
         ;;                  (dired-find-file))
         ;;              (dired-find-file))))
         ("E" . emisc-dired-multi-windows))) 
(use-package dired-collapse
  :straight t
  :after (dired))
(use-package dired-rainbow
  :straight t
  :after (dired))
(use-package dired-subtree
  :straight t
  ;; Ensure this loads after dired so it can override `dired-mode-map'
  ;; bindings.
  :after (dired)
  :bind (:map dired-mode-map
         ("<tab>" . dired-subtree-toggle)
         ("M-p" . dired-subtree-previous-sibling)
         ("M-n" . dired-subtree-next-sibling)
         ("M-u" . dired-subtree-up)))
(use-package dired-x
  ;; This allows, e.g. using ! in dired to smartly untar something. Without it
  ;; you have to manually enter the flags.
  :bind (("s-r" . dired-jump)
         ("C-x d" . nil)))
(use-package doc-view
  ;; Prevent squashing of ace-window binding.
  :bind (:map doc-view-mode-map
              ("C-r" . nil)))
(use-package dumb-jump
  :straight t
  ;; I would rather just set dumb-jump-mode defcustom to t instead of demanding
  ;; and manually calling it, but it doesn't result in dumb-jump-mode being
  ;; activated. You can tell because despite dumb-jump-mode being t, dumb-jump
  ;; doesn't appear with describe-mode.
  :demand
  :config
  (dumb-jump-mode)
  :bind (:map dumb-jump-mode-map
         ("C-!" . dumb-jump-go)
         ("C-@" . dumb-jump-back)
         ("C-#" . dumb-jump-quick-look)
         ("C-M-g" . nil)
         ("C-M-p" . nil)
         ("C-M-q" . nil)))
(use-package eldoc
  :delight)
(use-package elisp-mode
  :after (origami)
  :config
  (add-hook 'emacs-lisp-mode-hook
            (lambda ()
              (origami-mode)
              (lispy-mode))))
(use-package elpy
  :straight t
  :defer
  :after (python)
  ;; Unbind `elpy-shell-send-current-statement' because that Python process has
  ;; character encoding issues that seem to mess everything up and I frequently
  ;; hit C-return by accident.
  :bind (:map elpy-mode-map
              ("<C-return>" . nil))
  :config
  (elpy-enable)
  ;; If Emacs is started via, say, typing "emacs" in a bash shell, this var is
  ;; already set through .bashrc. Otherwise it doesn't get set so we set it
  ;; here.
  (setenv "WORKON_HOME" "~/envs"))
(use-package emisc-post
  :demand
  :straight t
  :after (ace-window avy origami multishell)
  :config
  (add-hook 'kill-emacs-hook 'emisc-enable-touchpad)
  
  (setq aw-dispatch-alist
        '((?x aw-delete-window "Del")
          (?c emisc-aw-swap-and-return "SwapR")            
          (?v emisc-aw-swap "Swap")
          (?m emisc-aw-uncover "Uncover")
          (?, emisc-aw-uncover-and-return "UncoverR")))
  
  ;; Autoload commands that have at least one entry point outside of the
  ;; bindings.
  :bind (("s-'" . emisc-switch-buffer)
         ("s-;" . eval-expression)
         ("s-0" . emisc-delete-window)
         ("s-b" . nil)                  ; reserved for starting emacs
         ("s-e" . emisc-jump-get-char-2-all-frames)
         ("s-h" . emisc-multishell-complete)
         ("s-k" . emisc-instakill-buffer)
         ("s-K" . emisc-exwm-kill-buffers)
         ("s-l" . emisc-store-register)
         ("s-m" . emisc-browser-startup)
         ("s-n" . emisc-jump-get-word-1-all-frames)
         ("s-o" . emisc-push-brainstack)
         ("s-O" . emisc-pop-brainstack)
         ("s-p" . emisc-toggle-touchpad)
         ("s-q" . hydra-emisc-exit/body)
         ("s-t" . emisc-ace-window-with-autowiden)
         ("s-u" . emisc-recall-register)
         ("s-v" . emisc-start-eww)
         ("s-w" . hydra-emisc-window/body)
         ("s-z" . hydra-emisc-scratch/body)
         ("M-w" . emisc-kill-ring-save-appendable)
         ("M-Q" . emisc-sym-magit-status)
         ("C-:" . emisc-ace-mc-add-single-cursor-word)
         ("C-\"" . emisc-ace-mc-add-single-cursor-char)
         ("C-M-;" . emisc-ace-mc-add-multiple-cursor-word)
         ("C-M-'" . emisc-ace-mc-add-multiple-cursor-char)
         ("C-M-^" . emisc-qrr-swap-opposites)
         ("C-M-y" . emisc-yank-pop)
         ("M-s-c" . emisc-toggle-clm)
         ("M-s-h" . emisc-blink)
         ("M-s-j" . emisc-gnome-screenshot)
         ("M-s-l" . emisc-flip-bool)
         ("M-s-w" . emisc-copy-bp-str)
         ("M-s-z" . emisc-tail-log)
         ("C-s-h" . emisc-insert-hack-block)
         ("C-s-S-h" . emisc-remove-hack-block)
         ("C-s-j" . emisc-org-toggle-isolate)
         ("C-s-o" . emisc-view-brainstack)
         ("C-s-q" . emisc-swap-two-marked-regions)
         ("C-s-r" . emisc-yank-rectangle-dwim)
         ("C-s-s" . emisc-eval-previous-src-block)
         ("C-s-t" . emisc-exwm-test-init)
         ("<s-mouse-1>" . emisc-exwm-restart)
         ("<s-mouse-3>" . emisc-exwm-restart-force)
         :map org-mode-map
         ("<s-tab>" . emisc-org-cycle)))
(use-package emisc-pre
  :straight t
  :demand)
(use-package emms
  :straight t)
(use-package epa-file)
(use-package eww
  :bind (:map eww-mode-map
         ("*" . emisc-eww-browse-external)))
(use-package expand-region
  :straight t
  :bind (("C-=" . er/expand-region)
         ("C-+" . er/contract-region)))
(use-package exwm
  :straight t
  :demand
  :after (emisc-pre xelb)
  :init
  ;; Kill other emacs instances when restarting exwm. This is heavy-handed,
  ;; kill9ing all other processes starting with "emacs" This was due to an
  ;; issue where the new instance would come up and the first one wouldn't
  ;; die. And if it did, the new instance would only cover a portion of the
  ;; screen representing its buffer at the time the first instance died or
  ;; something. A better way would be to run with just vanilla Emacs + EXWM and
  ;; see if I can reproduce it, and if not then bisect my config.
  (when (getenv emisc-exwm-restart-env)
    (setenv emisc-exwm-restart-env nil)
    (emisc-exwm-kill-other-emacsen))
  :config
  (cond ((eq 3 (length (emisc-monitor-names-l-to-r)))
         ;; Standard work 3-monitor setup
         (add-hook 'exwm-init-hook
                   (lambda ()
                     ;; Frame 0
                     (delete-other-windows)
                     (ignore-errors
                       (select-frame (nth 0 (emisc-frames-l-to-r))))
                     (split-window-right)
                     (split-window-right)
                     (find-file "~/org/rich.org")
                     (ignore-errors
                       (emisc-autowiden-toggle))
                     
                     ;; Frame 1
                     (ignore-errors
                       (select-frame (nth 1 (emisc-frames-l-to-r)))
                       (split-window-right))

                     ;; Frame 2
                     )))
        (t
         ;; Default setup
         ))
  (add-hook 'exwm-update-title-hook
            (lambda ()
              ;; Normally `exwm-workspace-rename-buffer' runs
              ;; `exwm-update-title-hook', so shadow the latter lest it recurse
              ;; infinitely.
              (let (exwm-update-title-hook)
                (when exwm-title
                  (exwm-workspace-rename-buffer (emisc-exwm-buffer-translate
                                                 exwm-title))))))
  ;; Even though scroll-bar-mode is -1, for some reason EXWM puts scroll bars
  ;; in the new frames, so disable them.
  (add-to-list 'after-make-frame-functions
               (lambda (frame) (scroll-bar-mode -1)) t)
  (add-hook 'exwm-manage-finish-hook
            (lambda ()
              (when (and exwm-class-name
                         (or (string= exwm-class-name "Firefox")
                             (string= exwm-class-name "Chromium-browser")))
                (exwm-input-set-local-simulation-keys
                 ;; avy with mouseless browsing extension
                 '(;; New window
                   ([?\C-c ?\C-e] . [?\C-n])
                   ;; Escape
                   ([?\C-g] . [escape])
                   ;; Prev tab
                   ([?\M-p] . [C-prior])
                   ;; Next tab
                   ([?\M-n] . [C-next])
                   ;; Shift tab prev-ward
                   ([?\M-P] . [C-S-prior])
                   ;; Shift tab next-ward
                   ([?\M-N] . [C-S-next])
                   ;; Kill tab
                   ([?\M-k] . [?\C-w])
                   ;; Find in page
                   ([?\C-s] . [?\C-f])
                   ;; Find - prev
                   ([?\C-\M-p] . [C-S-G])
                   ;; Find - next
                   ([?\C-\M-n] . [?\C-g])
                   ;; Back
                   ([?\C-\M-b] . [M-left])
                   ;; Forward
                   ([?\C-\M-f] . [M-right])
                   ;; Page down
                   ([?\M-v] . [prior])
                   ;; Page up
                   ([?\C-v] . [next])
                   ;; Scroll up a line
                   ([?\C-p] . [up])
                   ;; Scroll down a line
                   ([?\C-n] . [down])
                   ;; forward-char
                   ([?\C-f] . [right])
                   ;; backward-char
                   ([?\C-b] . [left])
                   ;; Home
                   ([?\M-<] . [home])
                   ;; End
                   ([?\M->] . [end])
                   ;; beginning-of-line
                   ([?\C-a] . [home])
                   ;; end-of-line
                   ([?\C-e] . [end])
                   ;; copy
                   ([?\M-w] . [?\C-c])
                   ;; Del
                   ([?\C-d] . [del])
                   ;; Copy
                   ([?\M-w] . [?\C-c])
                   ;; Paste
                   ([?\C-y] . [?\C-v])
                   ;; Undo
                   ([?\C-/] . [?\C-z])
                   ;; Redo
                   ([?\C-?] . [?\C-y]))))))
  (when (getenv emisc-exwm-testing-env)
    (setenv emisc-exwm-testing-env nil))
  (exwm-enable))
(use-package exwm-config
  :bind ("s-b" . exwm-input-toggle-keyboard))
(use-package exwm-randr
  :after (ace-window emisc-pre exwm)
  :config
  (add-hook 'exwm-randr-refresh-hook 'emisc-exwm-randr-refresh)
  (add-hook 'exwm-randr-screen-change-hook
            (lambda ()
              (setq exwm-randr-workspace-output-plist
                    (cl-loop for name in (emisc-monitor-names-l-to-r)
                             for i from 0 to exwm-workspace-number
                             collect i
                             collect name))
              (start-process-shell-command
               "xrandr" nil (concat "xrandr --output "
                                    (nth 0 (emisc-monitor-names-l-to-r))
                                    " --auto"))))
  (setq exwm-workspace-number (length (emisc-monitor-names-l-to-r)))
  (setq exwm-randr-workspace-output-plist
        (cl-loop for name in (emisc-monitor-names-l-to-r)
                 for i from 0 to exwm-workspace-number
                 collect i
                 collect name))
  (exwm-randr-enable))
(use-package exwm-systemtray
  :after (exwm)
  :config
  (exwm-systemtray-enable))
(use-package fancy-narrow
  :straight t)
(use-package files
  :bind (("C-x C-f" . nil)
         ("C-x C-c" . nil)
         ("s-g" . revert-buffer)
         ("C-x C-s" . nil)
         ("s-s" . save-buffer)
         ("C-x s" . nil)
         ("s-S" . save-some-buffers)))
(use-package fill
  :bind (("M-r" . emisc-fill-paragraph)))
(use-package flymake
  :bind (("C-c C-n" . flymake-goto-next-error)
         ("C-c C-p" . flymake-goto-prev-error)))
(use-package forge
  :after magit
  :straight t)
(use-package frame
  ;; Unbind suspend-frame because when I hit it accidentally it locks up Emacs
  ;; for a bit.
  :bind ("C-x C-z" . nil))
(use-package free-keys
  :straight t)
(use-package ghub
  :straight t)
(use-package git-gutter
  :straight t
  :delight
  ;; I could defer git-gutter but I would have to hook it into autowiden and this is easier.
  :demand
  :bind (("C-s-w" . git-gutter:next-hunk)
         ("C-s-S-w" . git-gutter:previous-hunk)
         ("C-s-k" . git-gutter:revert-hunk))
  ;; deleteme :config (advice-add 'window--display-buffer :after (lambda (&rest args) (when git-gutter-mode (git-gutter))))
  )
(use-package go-mode
  :straight t
  :after (origami)
  :config
  ;; Modify go-mode-hook
  (add-hook 'go-mode-hook
            (lambda ()
              ;; Add a gofmt call before saving a file, but only add it to the
              ;; local version of `before-save-hook'.
              (add-hook 'before-save-hook 'gofmt-before-save nil t)
              ;; Define the compile command.
              (set (make-local-variable 'compile-command)
                   "go build -v && go test -v && go vet")
              ;; Add company backend
              (set (make-local-variable 'company-backends) '(company-go))
              (company-mode)
              ;; Set the column width
              (set (make-local-variable 'fill-column) 120)
              ;; Turn on origami mode
              (origami-mode)
              (yas-minor-mode)
              (lsp)))
  ;; Commenting out go-flymake for now, because 1) it leaves flymake_* files
  ;; all over the place and I'm hoping lsp can do this instead.
  
  ;; There is no package for goflymake, so load it manually.
  ;; (let ((gopath (car (split-string
  ;;                     (shell-command-to-string "go env GOPATH")))))
  ;;   (add-to-list 'load-path (concatenate 'string
  ;;                                        gopath
  ;;                                        "/src/github.com/dougm/goflymake"))
  ;;   (require 'go-flymake))
  )
(use-package go-snippets
  :straight t)
(use-package gorepl-mode
  :straight t
  :after (go-mode)
  :bind (:map gorepl-mode-map
         ("C-s-r" . gorepl-hydra/body))
  :hook go-mode)
(use-package help
  :bind ("M-s-k" . describe-key))
(use-package hydra
  :straight t)
(use-package info
  :bind (("C-h i" . nil)
         ("M-s-i" . info)))
(defun disable-hl-line-mode ()
    (interactive)
    (setq-local global-hl-line-mode nil)
    (setq qwfparst 3)
    )
(use-package ivy
  :straight t
  :delight
  :init
  
  :hook (ivy-occur-mode . disable-hl-line-mode))
(use-package ivy-hydra
  :straight t)
(use-package keyfreq
  :straight t
  :config
  ;; Only enable keyfreq mode if this is the only Emacs process. Otherwise
  ;; multiple processes may attempt to lock the keyfreq file.
  (let* ((pids (list-system-processes))
         (proc-names (loop for pid in pids
                           collect (cdr (assoc 'comm
                                               (process-attributes pid)))))
         (num-emacs-processes (count "emacs" proc-names :test #'equal)))
    (when (= num-emacs-processes 1)
      (keyfreq-mode))))
(use-package lispy
  :straight t
  :delight
  ;; When loading lispy.el, `lispy-set-key-theme' is set to '(special lispy
  ;; c-digits). The following makes it so that c-digits are not included so
  ;; that numeric args can be used as normal. Not sure why this isn't just a
  ;; defcustom.
  :config
  (lispy-set-key-theme '(special lispy))
  :bind (:map lispy-mode-map
              ("M-m" . nil) ; back-to-indentation
              ("M-q" . nil) ; magit-status
              ("E" . special-lispy-outline-next)
              ("I" . special-lispy-outline-prev)
              ("O" . special-lispy-outline-goto-child)
              ("J" . special-lispy-eval-and-insert)
              ("K" . special-lispy-shifttab)
              ("L" . special-lispy-oneline)
              ("n" . special-lispy-left)
              ("e" . special-lispy-down)
              ("i" . special-lispy-up)
              ("o" . special-lispy-right)
              ("h" . special-lispy-new-copy)
              ("j" . special-lispy-eval)
              ("k" . special-lispy-tab)
              ("l" . special-lispy-other-mode)))
(use-package lsp-mode
  :straight t
  :demand
  :config
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection "gopls")
                    :major-modes '(go-mode)
                    :server-id 'gopls))
  ;; (add-hook 'go-mode-hook #'lsp)
  )
(use-package lsp-python
  :after (python lsp-mode)
  :defer t
  :straight t)
(use-package lsp-ui
  :after (lsp-mode)
  :straight t
  :bind (:map lsp-ui-mode-map
         ("M-s-e" . lsp-find-definition)
         ("M-s-E" . lsp-find-type-definition)
         ("M-s-n" . lsp-ui-find-next-reference)
         ("M-s-o" . lsp-ui-peek-find-references)
         ("M-s-p" . lsp-ui-find-prev-reference)
         ("M-s-s" . lsp-ui-sideline-toggle-symbols-info)
         ("M-s-y" . lsp-ui-imenu)
         ("M-s-d" . lsp-describe-thing-at-point)
         ("M-s-q" . lsp-describe-session)
         ("M-s-r" . lsp-rename))
  :commands lsp-ui-mode)
(use-package magit
  :straight t
  :bind (("M-q" . magit-status)
         :map magit-status-mode-map
         ("J" . emisc-magit-pull-master)))
(use-package magit-files
  :after magit
  ;; This was working in `magit-file-mode-map' but I noticed it broken around
  ;; 06/19, so I moved it to global. 
  :bind (("C-M-g" . magit-file-dispatch)
         ("C-s-c" . magit-blame-reverse)
         ("C-s-v" . magit-blame-removal)
         ("C-s-x" . magit-blame-addition)))
(use-package magit-section
  :after magit
  :bind (:map magit-diff-mode-map
         ("M-p" . emisc-magit-section-backward-sibling)
         ("M-n" . emisc-magit-section-forward-sibling)))
(use-package menu-bar
  :bind (("s-d" . toggle-debug-on-error)
         ("s-D" . toggle-debug-on-quit)))
(use-package multiple-cursors
  ;; Load mc-cycle-cursors so that C-v and M-v cycle between cursors instead of
  ;; scrolling the buffer. Couldn't figure out how to do this with autoloads,
  ;; since the commands I want autoloaded come from mc-mark-more.el which
  ;; doesn't require mc-cycle-cursors. And I couldn't put it in :config since
  ;; mc-mark-more.el doesn't even require multiple-cursors, so :config never
  ;; triggers.
  :init (require 'mc-cycle-cursors)
  :straight t
  :bind (("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)))
(use-package multishell
  :straight t)
(use-package ob-http
  :defer
  :straight t)
(use-package ob-ipython
  :defer
  :straight t)
(use-package omnisharp
  :straight t
  ;; :commands (omnisharp start-omnisharp-server)
  :config
  (add-to-list 'company-backends 'company-omnisharp)
  (add-hook 'csharp-mode-hook 'omnisharp-mode))
;; '(omnisharp-server-executable-path "/home/rich/my-omnisharp")
(use-package org
  :straight t
  :demand
  :bind (("C-s-l" . org-store-link)
         ("s-a" . org-agenda)
         ("s-c" . org-capture)
         :map org-mode-map
         ("RET" . (lambda () (interactive) (org-return t)))
         ("C-s-a" . org-promote-subtree)
         ("C-s-b" . org-backward-heading-same-level)
         ("C-c C-b" . nil)
         ("C-s-d" . emisc-org-tab-no-uncles)
         ("C-s-e" . org-demote-subtree)
         ("C-s-f" . org-forward-heading-same-level)
         ("C-c C-f" . nil)
         ("C-s-u" . emisc-org-up-heading)
         ("C-c C-u" . nil)
         ("C-s-p" . org-previous-visible-heading)
         ("C-c C-p" . nil)
         ("C-s-n" . org-next-visible-heading)
         ("C-c C-n" . nil)
         ("C-c t" . emisc-org-todo-toggle)
         ("C-c C-j" . emisc-org-goto))
  :config
  (setq org-plantuml-jar-path
        (expand-file-name "~/src/org/contrib/scripts/plantuml.jar"))
  ;; Enable EasyPG.
  (epa-file-enable)
  ;; After changing a PlantUML block, and pressing C-c C-c to update the inline
  ;; image generated by the block, by default only the link appears. To display
  ;; the image inline again one must press C-c C-x C-v to disable, then again
  ;; to re-enable inline images. Instead, ensure that the image is displayed
  ;; automatically.
  (add-hook 'org-babel-after-execute-hook
            'org-display-inline-images)
  (add-hook 'org-mode-hook (lambda () (toggle-truncate-lines 0))))
(use-package org-archive
  :after (org)
  :config
  (add-hook 'org-archive-hook (lambda () (save-buffer))))
(use-package org-bullets
  :straight t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
(use-package org-crypt
  :after (org)
  :config
  ;; Enable encryption of headlines with :crypt: tag on save.
  (org-crypt-use-before-save-magic))
(use-package org-clock
  :after (org))
(use-package org-id
  :after (org))
(use-package org-macs
  :after (org))
(use-package org-tempo
  :after (org))
(use-package origami
  :straight t
  :demand
  :bind (:map origami-mode-map
         ("C-t C-t" . origami-close-all-nodes)
         ("C-t C-a" . origami-open-all-nodes)
         ("C-t C-s" . origami-recursively-toggle-node)))
(use-package oauth2
  :straight t)
(use-package paradox
  :straight t
  :bind ("C-x P" . paradox-list-packages))
(use-package paredit
    :straight t)
(use-package persp-mode
  :straight t
  :demand)
(use-package powerline
    :straight t
    :config
    (powerline-default-theme))
(use-package plstore
  :straight t)
(use-package pylint
  :straight t)
(use-package proced
  :bind ("C-x p" . proced))
(use-package python
  :defer
  :config
  (add-hook 'python-mode-hook
            (lambda ()
              (setq fill-column 72)     ; per PEP 8
              ;; (define-key python-mode-map "\C-m" 'newline-and-indent)
              ;; IPython 5 changed to use prompt_toolkit, and the
              ;; --simple-prompt flag which works for ipython is not
              ;; implemented as a flag to ipython console as of this writing.
              (setenv "JUPYTER_CONSOLE_TEST" "1")))
  :bind (:map python-mode-map
         ("C-s-y" . emisc-transpose-py-arg)
         ("C-s-Y" . emisc-transpose-py-arg-back)))
(use-package python-info
  :straight t)
(use-package python-pep8
  :straight t)
(use-package python-pylint
  :straight t)
(use-package rainbow-mode
  :straight t)
(use-package replace
  :bind ("C-s-z" . query-replace-regexp))
(use-package savehist
  :demand
  :config
  (emisc-load-registers)
  (advice-add 'savehist-save :before (lambda (&rest args)
                                       (emisc-dump-registers)))
  (unless (get-register ?z)
    (message "THE Z REGISTER IS EMPTY")))
(use-package shell
  ;; `shell-mode-hook' refers to a defun in emisc-pre
  :after emisc-pre
  :bind (:map shell-mode-map
         ("C-c C-k" . comint-kill-subjob)
         ("C-S-p" . comint-previous-prompt)
         ("C-S-n" . comint-next-prompt)))
(use-package simple
  :bind (("M-t" . transpose-sexps)
         ("M-s-t" . toggle-truncate-lines)
         ("C-x C-u" . upcase-dwim)
         ;; Unbind C-@ which is redundant with C-SPC.
         ("C-@" . nil)
         ("M-x" . nil))) ; heresy
(use-package slack
  :straight t
  ;; It is critical that this package be deferred. Otherwise it causes an issue
  ;; where the exwm prompt "Replace existing window manager?" is automatically
  ;; told no.
  :after (circe diary-lib plstore oauth2 websocket)
  :defer t
  ;; This package's init seems very fragile. Beware making any changes below.
  :commands (slack-start)

  :config
  (slack-register-team
   :name "Turnitin"
   :default t
   :client-id "4594535297.405943561287"
   :client-secret "9d1626c2eb7890dfd4b294c72aa26cd9"
   :token "xoxp-4594535297-392317465621-406275691731-34aee9f561ca70053ef79706d95f758b"
   :subscribed-channels '(general nos_team oakland random richkindertest seu_public seu_threads techies tfs-team tribe5 tribe5-alerts)
   :full-and-display-names t)
  (defun slack-update-all ()
    (interactive)
    (slack-im-list-update)
    (slack-channel-list-update)
    (slack-group-list-update))
  (slack-update-all))
(use-package swiper
  :straight t
  :bind (("C-s" . swiper)
         ("C-S-s" . ivy-resume)))
(use-package sx
  :straight t)
(use-package time
  :config
  (display-time-mode))
(use-package uniquify)
(use-package visual-regexp
  :straight t)
(use-package visual-regexp-steroids
  :straight t)
(use-package web-mode
  :straight t
  :config
  (progn
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (setq web-mode-engines-alist '(("django" . "\\.html\\'")))))
(use-package websocket
  :straight t)
(use-package whole-line-or-region
  :straight t
  :delight)
(use-package wgrep
  :straight t)
(use-package which-key
  :straight t)
(use-package window
  :bind (("C-x 2" . nil)
         ("C-x 3" . nil)
         ("s-)" . delete-other-windows)
         ("s-2" . split-window-below)
         ("s-3" . split-window-right)
         ("s-@" . emisc-split-window-above)
         ("s-#" . emisc-split-window-left)))
(use-package winner
  :bind (("s-/" . winner-undo)
         ("s-?" . winner-redo)))
(use-package xelb
  :straight t)
(use-package yaml-mode
  :straight t
  :init
  (add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . yaml-mode)))
(use-package zenburn-theme
  :straight t
  :config
  (load-theme 'zenburn t))

;;; Miscellaneous settings.
(defalias 'yes-or-no-p 'y-or-n-p)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(global-set-key (kbd "<menu>") nil) ;; laptop
(define-coding-system-alias 'UTF-8 'utf-8)
(define-coding-system-alias 'utf8 'utf-8) ;; FreeCAD source
(set-frame-parameter nil 'fullscreen 'maximized)
(put 'narrow-to-region 'disabled nil)
(put 'magit-diff-edit-hunk-commit 'disabled nil)
(put 'magit-edit-line-commit 'disabled nil)
