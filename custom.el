
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#3F3F3F" :foreground "#DCDCCC" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 105 :width normal :foundry "PfEd" :family "DejaVu Sans Mono"))))
 '(dired-subtree-depth-1-face ((t (:background "gray20"))))
 '(dired-subtree-depth-2-face ((t (:background "gray16"))))
 '(dired-subtree-depth-3-face ((t (:background "gray12"))))
 '(dired-subtree-depth-4-face ((t (:background "gray8"))))
 '(dired-subtree-depth-5-face ((t (:background "gray4"))))
 '(dired-subtree-depth-6-face ((t (:background "gray0"))))
 '(lsp-ui-sideline-global ((t (:background "gray14"))))
 '(magit-diff-added-highlight ((t (:inherit diff-added))))
 '(magit-diff-context-highlight ((t (:background "#595959" :foreground "grey70"))))
 '(magit-diff-removed-highlight ((t (:inherit diff-removed))))
 '(mode-line-inactive ((t (:background "#383838" :foreground "peru" :box (:line-width -1 :style released-button)))))
 '(org-checkbox ((t (:background "#5F5F5F" :foreground "#FFFFEF" :box nil)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ace-window-display-mode t)
 '(ansi-color-names-vector
   ["#3F3F3F" "#CC9393" "#7F9F7F" "#F0DFAF" "#8CD0D3" "#DC8CC3" "#93E0E3" "#DCDCCC"])
 '(auto-save-default nil)
 '(auto-save-interval 0)
 '(auto-save-timeout 0)
 '(auto-save-visited-interval 10000)
 '(avy-all-windows 'all-frames)
 '(avy-dispatch-alist
   '((75 . avy-action-kill-move)
     (83 . avy-action-kill-stay)
     (84 . avy-action-teleport)
     (77 . avy-action-mark)
     (67 . avy-action-copy)
     (121 . avy-action-yank)
     (122 . avy-action-zap-to-char)))
 '(avy-keys
   '(97 114 115 116 110 101 105 111 119 102 117 113 112 108 59 118 109 47 120 99 46))
 '(avy-style 'de-bruijn)
 '(aw-background nil)
 '(aw-dispatch-always t)
 '(aw-keys
   '(97 114 115 116 110 101 105 111 102 119 117 121 113 112 108 59 47 44))
 '(aw-leading-char-style 'char)
 '(backup-directory-alist '(("." . "~/.emacs.d/backups")))
 '(bart-manage-window t)
 '(bart-station '19th)
 '(beacon-mode nil)
 '(blink-cursor-mode nil)
 '(bookmark-save-flag 1)
 '(browse-url-browser-function 'browse-url-firefox)
 '(browse-url-firefox-arguments '("--new-window"))
 '(c-default-style
   '((c++-mode . "gnu")
     (java-mode . "java")
     (awk-mode . "awk")
     (other . "python")))
 '(clean-buffer-list-delay-general 4)
 '(command-log-mode-is-global t)
 '(command-log-mode-open-log-turns-on-mode t)
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(counsel-ag-base-command "ag --nocolor --nogroup -z %s")
 '(counsel-mode t)
 '(counsel-rg-base-command "rg -S --no-heading --line-number --color never %s -u .")
 '(csv-separators '("," "	" ";"))
 '(custom-file "~/.emacs.d/custom.el")
 '(custom-safe-themes
   '("080fd60366fb1d6e7aea9f8fd0de03e2a40ac995e51b1ed21de37431d43b4d88" "b2a36e3137cbe8025aaf52937f9b20ba295cc5c2874b40f3f7153228ec61abb4" "276388f660bcdae72048818c80af72ec9d144f6f261c8b733c339db95de4ba65" default))
 '(debug-on-error nil)
 '(debug-on-quit nil)
 '(default-frame-alist '((cursor-color . "white") (cursor-type . bar)))
 '(dired-clean-up-buffers-too nil)
 '(dired-recursive-copies 'always)
 '(dirtrack-list
   '("^\\(([A-z]*) \\)?\\([^@]+\\)@\\([^:]+\\):\\([^$]*\\)$ " 4))
 '(display-time-mode t)
 '(edebug-print-length 10000)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(ein:connect-default-notebook "8888/DefaultNB")
 '(ein:console-executable
   '(("http://127.0.0.1:8888" . "/usr/local/bin/ipython")
     ("http://127.0.0.1:8889" . "/home/rich/envs/groupmaker/bin/ipython")))
 '(ein:notebook-autosave-frequency 3600)
 '(ein:notebook-modes '(ein:notebook-python-mode))
 '(ein:propagate-connect nil)
 '(ein:url-or-port '("http://127.0.0.1:8888" "http://127.0.0.1:8889"))
 '(elpy-rpc-backend "jedi")
 '(elpy-rpc-python-command "python3")
 '(elpy-rpc-timeout 15)
 '(emisc-autoshells
   '(("localhost" "" "~/"
      ("shell_l1" "shell_l2"))
     ("seu1.s2prod" "rkinder" "/home/rkinder/"
      ("shell_2r1" "shell_2r2"))
     ("c6" "rkinder" "/home/rkinder/"
      ("shell_1r1" "shell_1r2"))
     ("seu-jenkins301.ca2prd" "rkinder" "/home/rkinder"
      ("shell_3r1" "shell_3r2"))))
 '(emisc-autoshells-restart-after-init-p nil)
 '(emisc-autoshells-restart-after-instakill-p nil)
 '(emisc-autowiden-p nil)
 '(emisc-chromium-urls nil)
 '(emisc-exwm-buffer-translations
   '(("iParadigms/\\([A-z0-9]*\\):\\ \\(.*\\)\\ -\\ Mozilla\\ Firefox" . "(ghe \\1 - \\2)")
     ("iParadigms.*" . "(ghe)")
     ("\\(.*\\)\\ -\\ YouTube\\ -\\ Mozilla\\ Firefox" . "(youtube - \\1)")
     ("Turnitin\\ Mail\\ -\\ Inbox\\ -\\ Mozilla\\ Firefox" . "(gmail Turnitin)")
     ("Gmail\\ -\\ Inbox\\ -\\ Mozilla\\ Firefox" . "(gmail me)")
     ("Turnitin\\ -\\ Sign\\ In\\ -\\ Mozilla\\ Firefox" . "(okta sign in)")
     ("Turnitin\\ -\\ My\\ Applications\\ -\\ Mozilla\\ Firefox" . "(okta)")
     ("Insights:\\ \\(.*\\)\\ -\\ Mozilla\\ Firefox" . "(nri \\1)")
     ("\\(.*\\)\\ \\-\\ Alerts\\ by\\ New\\ Relic\\ -\\ Mozilla\\ Firefox" . "(nra \\1)")
     ("\\(.*\\)\\ -\\ Mozilla\\ Firefox" . "(\\1)")))
 '(emisc-firefox-urls
   '("http://turnitin.slack.com" "https://calendar.google.com/" "https://ghe.iparadigms.com/iParadigms/" "https://youtube.com" "https://mail.google.com/mail/" "http://seu-jenkins301.ca2prd:8080/" "https://turnitin.atlassian.net/" "https://rpm.newrelic.com/accounts/1080775" "https://splunk.live.iparadigms.com/" "https://turnitin.okta.com/app/UserHome" "https://careers.smartrecruiters.com/TurnitinLLC?search=support" "https://jenkins.turnitin.org/job/SEU/"))
 '(emisc-keyboard-layout 'colemak)
 '(emisc-scratch-template '("~/org/scratch" "scratch"))
 '(emisc-screenshot-template '("~/org/screenshots" "pic" "png"))
 '(emisc-touchpad-str "VirtualPS/2 VMware VMMouse")
 '(emisc-touchpad-xinput-num 14)
 '(emms-source-file-default-directory "/home/rkinder/Music")
 '(epg-gpg-program "gpg2")
 '(eshell-status-in-mode-line nil)
 '(eval-expression-print-length 1000)
 '(exec-path
   '("/home/rkinder/bin" "/usr/local/bin" "/usr/local/sbin" "/usr/bin" "/usr/sbin" "/bin" "/sbin" "/home/rkinder/go/bin" "/home/rkinder/.local/bin" "/home/rkinder/go/bin" "/home/rkinder/bin" "/usr/local/libexec/emacs/27.0.50/x86_64-pc-linux-gnu" "/home/rkinder/.cargo/bin"))
 '(explicit-shell-file-name "/bin/bash")
 '(exwm-input-global-keys
   '(([8388623]
      . emisc-view-brainstack)
     ([142606434]
      . counsel-descbinds)
     ([142606435]
      . emisc-toggle-clm)
     ([142606438]
      . counsel-describe-function)
     ([142606441]
      . info)
     ([142606442]
      . emisc-gnome-screenshot)
     ([142606443]
      . describe-key)
     ([142606444]
      . emisc-flip-bool)
     ([142606453]
      . counsel-unicode-char)
     ([142606454]
      . counsel-describe-variable)
     ([142606458]
      . emisc-tail-log)
     ([8388676]
      . toggle-debug-on-quit)
     ([8388647]
      . emisc-switch-buffer)
     ([8388655]
      . winner-undo)
     ([8388656]
      . emisc-delete-window)
     ([8388657]
      . delete-other-windows)
     ([8388658]
      . split-window-below)
     ([8388672]
      . emisc-split-window-above)
     ([8388643]
      . emisc-split-window-left)
     ([8388659]
      . split-window-right)
     ([8388660]
      . counsel-git)
     ([8388661]
      . counsel-git-grep)
     ([8388662]
      . counsel-rg)
     ([8388663]
      . emisc-counsel-locate-here)
     ([8388664]
      . counsel-linux-app)
     ([8388667]
      . eval-expression)
     ([8388671]
      . winner-redo)
     ([8388705]
      . org-agenda)
     ([8388706]
      . exwm-input-toggle-keyboard)
     ([8388707]
      . org-capture)
     ([8388708]
      . toggle-debug-on-error)
     ([8388718]
      . emisc-jump-get-word-1-all-frames)
     ([8388709]
      . emisc-jump-get-char-2-all-frames)
     ([8388710]
      . counsel-find-file)
     ([8388711]
      . revert-buffer)
     ([8388712]
      . emisc-multishell-complete)
     ([8388713]
      . emisc-counsel-bookmark)
     ([8388715]
      . emisc-instakill-buffer)
     ([8388683]
      . emisc-exwm-kill-buffers)
     ([8388716]
      . emisc-store-register)
     ([8388717]
      . emisc-browser-startup)
     ([8388719]
      . emisc-push-brainstack)
     ([8388687]
      . emisc-pop-brainstack)
     ([8388720]
      . emisc-toggle-touchpad)
     ([8388721]
      . hydra-emisc-exit/body)
     ([8388722]
      . dired-jump)
     ([8388691]
      . save-some-buffers)
     ([8388724]
      . emisc-ace-window-with-autowiden)
     ([8388725]
      . emisc-recall-register)
     ([8388726]
      . emisc-start-eww)
     ([8388694]
      . emisc-start-eww-uniquely)
     ([8388727]
      . hydra-emisc-window/body)
     ([8388728]
      . counsel-M-x)
     ([8388730]
      . hydra-emisc-scratch/body)))
 '(exwm-input-prefix-keys nil)
 '(exwm-layout-show-all-buffers t)
 '(exwm-workspace-show-all-buffers t)
 '(fancy-narrow-mode t)
 '(fci-rule-color "#383838")
 '(fill-column 79)
 '(forge-alist
   '(("github.com" "api.github.com" "github.com" forge-github-repository)
     ("gitlab.com" "gitlab.com/api/v4" "gitlab.com" forge-gitlab-repository)
     ("salsa.debian.org" "salsa.debian.org/api/v4" "salsa.debian.org" forge-gitlab-repository)
     ("codeberg.org" "codeberg.org/api/v1" "codeberg.org" forge-gitea-repository)
     ("code.orgmode.org" "code.orgmode.org/api/v1" "code.orgmode.org" forge-gogs-repository)
     ("bitbucket.org" "api.bitbucket.org/2.0" "bitbucket.org" forge-bitbucket-repository)
     ("git.savannah.gnu.org" nil "git.savannah.gnu.org" forge-cgit*-repository)
     ("git.kernel.org" nil "git.kernel.org" forge-cgit-repository)
     ("repo.or.cz" nil "repo.or.cz" forge-repoorcz-repository)
     ("git.suckless.org" nil "git.suckless.org" forge-stagit-repository)
     ("git.sr.ht" nil "git.sr.ht" forge-srht-repository)
     ("ghe.iparadigms.com" "ghe.iparadigms.com/api" "ghe.iparadigms.com" forge-github-repository)))
 '(free-keys-modifiers '("" "s" "C" "C-s" "M" "M-s" "C-M" "C-M-s"))
 '(global-company-mode t)
 '(global-git-gutter-mode t)
 '(global-hl-line-mode t)
 '(global-prettify-symbols-mode nil)
 '(global-undo-tree-mode t)
 '(gofmt-command "~/go/bin/goimports")
 '(icicle-expand-input-to-common-match 2)
 '(icicle-mode t)
 '(iedit-toggle-key-default nil)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice nil)
 '(initial-major-mode 'text-mode)
 '(initial-scratch-message nil)
 '(ivy-action-wrap t)
 '(ivy-extra-directories nil)
 '(ivy-mode t)
 '(ivy-sort-matches-functions-alist
   '((t . ivy--prefix-sort)
     (ivy-switch-buffer . ivy-sort-function-buffer)))
 '(keyfreq-autosave-mode t)
 '(keyfreq-file "~/.emacs.d/keyfreq")
 '(lsp-clients-go-server "gopls")
 '(lsp-ui-doc-enable nil)
 '(lsp-ui-flycheck-enable t)
 '(lsp-ui-sideline-enable t)
 '(magit-commit-show-diff nil)
 '(magit-default-tracking-name-function 'magit-default-tracking-name-branch-only)
 '(magit-diff-refine-hunk 'all)
 '(magit-display-buffer-function 'emisc-magit-display-buffer)
 '(magit-git-executable "/home/rkinder/bin/git")
 '(magit-git-global-arguments
   '("--literal-pathspecs" "-c" "core.preloadindex=true" "-c" "pager.diff=true"))
 '(magit-log-arguments '("--graph" "--decorate" "-n256" "--color"))
 '(magit-log-auto-more t)
 '(magit-log-margin '(t age-abbreviated magit-log-margin-width t 12))
 '(magit-push-always-verify nil)
 '(magit-repository-directories
   '(("~/emisc" . 0)
     ("~/org" . 0)
     ("~/.emacs.d" . 0)
     ("~/go/src/ghe.iparadigms.com/iParadigms" . 1)))
 '(magit-revision-show-gravatars t)
 '(magit-view-git-manual-method 'man)
 '(magithub-github-hosts '("github.com" "ghe.iparadigms.com"))
 '(menu-bar-mode nil)
 '(message-kill-buffer-on-exit t)
 '(message-send-mail-function 'smtpmail-send-it)
 '(mu4e-bookmarks
   '(("maildir:\"/[Gmail].All Mail\" AND flag:unread AND NOT flag:trashed" "Unread messages" 117)
     ("maildir:\"/[Gmail].All Mail\" AND date:today..now" "Today's messages" 116)
     ("maildir:\"/[Gmail].All Mail\" AND date:7d..now" "Last 7 days" 119)
     ("maildir:\"/[Gmail].All Mail\" AND mime:image/*" "Messages with images" 112)))
 '(mu4e-compose-signature nil)
 '(mu4e-drafts-folder "/[Gmail].Drafts")
 '(mu4e-get-mail-command "offlineimap")
 '(mu4e-html2text-command "w3m")
 '(mu4e-maildir-shortcuts
   '(("/INBOX" . 105)
     ("/[Gmail].Sent Mail" . 115)
     ("/[Gmail].Trash" . 116)
     ("/[Gmail].All Mail" . 97)))
 '(mu4e-sent-folder "/[Gmail].Sent Mail")
 '(mu4e-sent-messages-behavior 'delete)
 '(mu4e-trash-folder "/[Gmail].Trash")
 '(multishell-history-entry-tracks-current-directory nil)
 '(nrepl-message-colors
   '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
 '(omnisharp-server-executable-path "/home/rkinder/omnisharp/run")
 '(org-agenda-custom-commands
   '(("n" "Agenda and all TODOs"
      ((agenda "" nil)
       (alltodo "" nil))
      nil)
     ("o" "ONGOING"
      ((todo "ONGOING" nil))
      nil)))
 '(org-agenda-files '("/home/rkinder/org/rich.org"))
 '(org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (plantuml . t)
     (shell . t)
     (ditaa . t)
     (http . t)))
 '(org-bullets-bullet-list '("◉" "○" "✸" "✿" "✚" "♠" "♥" "◆" "♣" "☢" "◇"))
 '(org-capture-templates
   '(("e" "Emacs")
     ("et" "Todo" entry
      (id "0f39a4ff-9432-472f-8cad-c5d19a6b52a3")
      "** TODO [#C] %?" :prepend t)
     ("el" "Learning" entry
      (id "e48816ab-effc-4baf-95f2-1abd1e4565d7")
      "** ONGOING %?" :prepend t)
     ("m" "Music" entry
      (id "df22ea1d-4f0e-485c-9a92-f19024c0d586")
      "**** %?" :prepend t)
     ("s" "SEU")
     ("sg" "Question - General" entry
      (id "615f66b1-449c-475d-828d-39cb012de19c")
      "** QUESTION %?" :prepend t)
     ("sr" "Question - Retro" entry
      (id "9e3a261b-6c13-4282-803e-855edffefdc4")
      "**** QUESTION %?" :prepend t)
     ("st" "Todo" entry
      (id "5ac8282b-21cc-49ac-a2ee-2abd44f8f2bb")
      "*** TODO [#C] %?" :prepend t)
     ("t" "Todo" entry
      (id "be3f3e33-45c7-43c4-a360-98a27c8da5f7")
      "** TODO [#C] %?" :prepend t)
     ("x" "Christmas Gift Ideas")
     ("xp" "Dad" entry
      (id "a7caf18d-fd58-4405-8136-bb64e7b48a6f")
      "***** %?" :prepend t)
     ("xd" "Dan" entry
      (id "d8472ca7-489c-4716-92f9-944c46985333")
      "***** %?" :prepend t)
     ("xf" "Frances" entry
      (id "4f76c69f-0653-410b-a845-9b8623f890ae")
      "***** %?" :prepend t)
     ("xm" "Mom" entry
      (id "500703d1-adf3-4b6a-b700-90c33577eced")
      "***** %?" :prepend t)
     ("xr" "Randy" entry
      (id "cf4f66bb-dd2a-4381-85d6-9950cb7666d6")
      "***** %?" :prepend t)))
 '(org-catch-invisible-edits 'show)
 '(org-confirm-babel-evaluate nil)
 '(org-crypt-key nil)
 '(org-default-notes-file "~/org/.notes.org")
 '(org-directory "~/org")
 '(org-ditaa-jar-path
   "/home/rkinder/.emacs.d/straight/repos/org/contrib/scripts/ditaa.jar")
 '(org-enforce-todo-checkbox-dependencies t)
 '(org-enforce-todo-dependencies t)
 '(org-goto-interface 'outline-path-completion)
 '(org-goto-max-level 12)
 '(org-id-link-to-org-use-id 'create-if-interactive)
 '(org-image-actual-width nil)
 '(org-mobile-directory "~/Dropbox/MobileOrg")
 '(org-outline-path-complete-in-steps nil)
 '(org-plantuml-jar-path "~/plantuml.jar")
 '(org-priority-faces '((65 . "#ee5555") (66 . "#dd7777") (67 . "#cc9393")))
 '(org-refile-targets '((nil :maxlevel . 8)))
 '(org-refile-use-outline-path t)
 '(org-src-fontify-natively t)
 '(org-src-tab-acts-natively t)
 '(org-startup-with-inline-images t)
 '(org-tags-column -76)
 '(org-tags-exclude-from-inheritance '("crypt"))
 '(org-todo-keyword-faces
   '(("WAIT" . "gold")
     ("QUESTION" . "goldenrod")
     ("ONGOING" . "slateblue3")
     ("SOMEDAY" . "lightslategrey")
     ("TODONT" . "DimGrey")))
 '(org-todo-keywords
   '((type "TODO(t)" "WAIT(w)" "SOMEDAY(s)" "ONGOING(o)" "QUESTION(q)" "|" "DONE(d)" "TODONT(x)")))
 '(origami-parser-alist
   '((java-mode . origami-java-parser)
     (c-mode . origami-c-parser)
     (c++-mode . origami-c-style-parser)
     (perl-mode . origami-c-style-parser)
     (cperl-mode . origami-c-style-parser)
     (js-mode . origami-c-style-parser)
     (js2-mode . origami-c-style-parser)
     (js3-mode . origami-c-style-parser)
     (go-mode . origami-c-style-parser)
     (php-mode . origami-c-style-parser)
     (python-mode . origami-python-parser)
     (emacs-lisp-mode . emisc-elisp-parser)
     (lisp-interaction-mode . origami-elisp-parser)
     (clojure-mode . origami-clj-parser)
     (triple-braces .
                    #[257 "\303\304\305\306\307\300\301\302$\310\"\311\312%\207"
                          ["{{{" "}}}" "\\(?:\\(?:{{{\\|}}}\\)\\)" make-byte-code 257 "\304\302\"\305\303\300\301$\207" vconcat vector
                           [origami-get-positions origami-build-pair-tree]
                           7 "

(fn CONTENT)"]
                          10 "

(fn CREATE)"])))
 '(outline-minor-mode-prefix "")
 '(package-archive-priorities '(("elpa" . 3) ("melpa" . 2) ("marmalade" . 1)))
 '(package-archives
   '(("gnu" . "http://elpa.gnu.org/packages/")
     ("melpa" . "http://melpa.milkbox.net/packages/")
     ("marmalade" . "http://marmalade-repo.org/packages/")))
 '(package-enable-at-startup nil)
 '(package-selected-packages
   '(multishell ace-window command-log-mode gorepl-mode company-lsp lsp-python lsp-ui lsp-mode yasnippet counsel-tramp ample-regexps org-bullets oauth2 lsp-go git-gutter paradox dumb-jump which-key wgrep bart-mode dockerfile-mode cython-mode elpy free-keys req-package use-package company-jedi lispy matlab-mode powerline ivy-hydra zenburn-theme counsel swiper company omnisharp csv-mode whole-line-or-region workgroups2 web-mode w3m visual-regexp-steroids undo-tree rainbow-mode python-pylint python-pep8 python-info pylint pep8 paredit keyfreq google-maps expand-region ein color-theme beacon avy))
 '(paradox-github-token t)
 '(pdf-view-midnight-colors '("#DCDCCC" . "#383838"))
 '(pop-up-windows nil)
 '(powerline-default-separator 'curve)
 '(powerline-display-mule-info nil)
 '(python-environment-virtualenv
   '("virtualenv" "--python=/usr/bin/python3" "--system-site-packages" "--quiet"))
 '(python-shell-interpreter-args "--pylab")
 '(remote-file-name-inhibit-cache nil)
 '(ring-bell-function 'ignore)
 '(safe-local-variable-values
   '((eval font-lock-add-keywords nil
           '(("(\\(dired-filter-define\\)[[:blank:]]+\\(.+\\)"
              (1 'font-lock-keyword-face)
              (2 'font-lock-function-name-face))))
     (git-commit-major-mode . git-commit-elisp-text-mode)
     (buffer-auto-save-file-name)))
 '(same-window-buffer-names '("*Help*" "*Apropos*"))
 '(savehist-additional-variables '(multishell-history emisc-brainstack emisc-register-alist))
 '(savehist-mode t)
 '(scroll-bar-mode nil)
 '(scroll-conservatively 1)
 '(scroll-margin 0)
 '(scroll-preserve-screen-position t)
 '(shell-cd-regexp "\\(cd\\)\\|\\(workon\\)")
 '(shell-mode-hook
   '((lambda nil
       (dirtrack-mode 1))
     (lambda nil
       (shell-dirtrack-mode -1))
     emisc-shell-fix-ps1))
 '(show-paren-mode t)
 '(shr-use-fonts nil)
 '(slack-buffer-emojify t)
 '(slack-prefer-current-team t)
 '(smtpmail-default-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(smtpmail-stream-type 'starttls)
 '(straight-recipe-overrides
   '((nil
      (emisc-post :files
                  ("emisc-post.el")
                  :repo "git@bitbucket.org:richkinder/emisc" :local-repo "emisc")
      (emisc-pre :files
                 ("emisc-pre.el")
                 :repo "git@bitbucket.org:richkinder/emisc" :local-repo "emisc")
      (forge :files
             ("lisp/*.el" "docs/forge.texi" "forge-pkg.el")
             :host github :repo "magit/forge" :fork
             (:host nil :repo "git@github.com:richkinder/forge"))
      (ghub :flavor melpa :host github :repo "magit/ghub" :fork
            (:host nil :repo "git@github.com:richkinder/ghub"))
      (multishell :host github :repo "emacs-straight/multishell" :fork
                  (:host nil :repo "git@github.com:richkinder/multishell")))))
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(tramp-auto-save-directory "~/.emacs.d/backups")
 '(tramp-completion-reread-directory-timeout nil)
 '(tramp-default-method "ssh")
 '(tramp-default-method-alist
   '((nil "%" "smb")
     ("\\`\\(127\\.0\\.0\\.1\\|::1\\|localhost6?\\|richbuntu\\)\\'" "\\`root\\'" "su")
     (nil "\\`\\(anonymous\\|ftp\\)\\'" "ftp")
     ("\\`ftp\\." nil "ftp")))
 '(tramp-default-proxies-alist
   '(("seu-jenkins301\\.ca2prd" "root" "/ssh:seu-jenkins301.ca2prd:")
     ("\\`npmd-vision2\\'" nil "/ssh:nmdadmin@npmd-vision2.scs.agilent.com:")
     ("\\`npmd-vision1\\'" nil "/ssh:nmdadmin@npmd-vision1.scs.agilent.com:")
     ("\\`gm\\'" nil "/ssh:root@gm_server:/")))
 '(tramp-verbose 3)
 '(undo-tree-mode-lighter " UT")
 '(uniquify-buffer-name-style 'post-forward nil (uniquify))
 '(url-debug t)
 '(use-empty-active-region t)
 '(use-package-compute-statistics t)
 '(user-mail-address "kinderrich@gmail.com")
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   '((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3")))
 '(vc-annotate-very-old-color "#DC8CC3")
 '(vc-handled-backends nil)
 '(w3m-add-referer 'lambda)
 '(w3m-confirm-leaving-secure-page nil)
 '(w3m-cookie-accept-domains '("127.0.0.1"))
 '(w3m-search-default-engine "duckduckgo")
 '(w3m-search-word-at-point nil)
 '(wdired-allow-to-change-permissions t)
 '(wg-buffer-local-variables-alist
   '((major-mode nil wg-deserialize-buffer-major-mode)
     (mark-ring wg-serialize-buffer-mark-ring wg-deserialize-buffer-mark-ring)
     (left-fringe-width nil nil)
     (right-fringe-width nil nil)
     (fringes-outside-margins nil nil)
     (left-margin-width nil nil)
     (right-margin-width nil nil)
     (vertical-scroll-bar nil nil)))
 '(wg-emacs-exit-save-behavior nil)
 '(wg-modeline-string "")
 '(wg-prefix-key "")
 '(wg-remember-frame-for-each-wg t)
 '(wg-session-file "~/.emacs.d/.wg_file")
 '(wg-workgroups-mode-exit-save-behavior nil)
 '(wgrep-auto-save-buffer t)
 '(which-key-lighter "")
 '(which-key-mode t)
 '(whole-line-or-region-extensions-alist
   '((copy-region-as-kill whole-line-or-region-copy-region-as-kill nil)
     (kill-region whole-line-or-region-kill-region nil)
     (kill-ring-save whole-line-or-region-kill-ring-save nil)
     (yank whole-line-or-region-yank nil)
     (emisc-kill-ring-save-appendable emisc-wlr-kill-ring-save-appendable nil)))
 '(whole-line-or-region-mode t)
 '(winner-mode t)
 '(workgroups-mode nil)
 '(yaml-mode-hook
   '(yaml-set-imenu-generic-expression highlight-indentation-mode)))
