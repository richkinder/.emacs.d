;; Load customization file, ensuring that `package-enable-at-startup' is set
;; properly, otherwise `package-initialize' will be called before init.el has a
;; chance to disable it.
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)
